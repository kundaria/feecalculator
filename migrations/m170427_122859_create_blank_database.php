<?php
 

use yii\db\Migration;
use yii\db\sqlite\Schema;

class m170427_122859_create_blank_database extends Migration
{
    public function safeUp()
    {
        $this->createTable('countries', [
            'id' => Schema::TYPE_BIGPK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'sortname' => Schema::TYPE_STRING,
        ]);
        
        $this->createTable('state', [
            'id' => Schema::TYPE_BIGPK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'country_id' => Schema::TYPE_BIGINT. ' NOT NULL',
        ]);
        
         // creates index for column `country_id`
        $this->createIndex(
            'idx-state-country',
            'state',
            'country_id'
        );
        
        // add foreign key for table `countries`
        $this->addForeignKey(
            'fk-state-country',
            'state',
            'country_id',
            'countries',
            'id',
            'RESTRICT'
        );
        
        
        // GENERATE UNIVERSITY 
        $this->createTable('universities', [
            'id' => Schema::TYPE_BIGPK,
            'name' => ' VARCHAR(500) NOT NULL',
            'province_id' => Schema::TYPE_BIGINT,
        ]);
        
        // creates index for column `province_id`
        $this->createIndex(
            'idx-university-state',
            'universities',
            'province_id'
        );
        
        // add foreign key for table `universities`
        $this->addForeignKey(
            'fk-university-state',
            'universities',
            'province_id',
            'state',
            'id',
            'RESTRICT'
        );
        
        
        // GENERATE PROGRAM TABLE 
        $this->createTable('programs', [
            'id' => Schema::TYPE_BIGPK,
            'name' => ' VARCHAR(500) NOT NULL',
        ]);
        
        // GENERASTE CALCULATION TABLE 
        $this->createTable('calculatoins', [
            'id' => Schema::TYPE_BIGPK,
            'university_id' => Schema::TYPE_BIGINT. ' NOT NULL',
            'program_id' => Schema::TYPE_BIGINT. ' NOT NULL',
            'tution_international' => Schema::TYPE_FLOAT. ' NOT NULL',
            'tution_canada' => Schema::TYPE_FLOAT. ' NOT NULL',
            'tution_province' => Schema::TYPE_FLOAT. ' NOT NULL',
            'total_fees' => Schema::TYPE_FLOAT. ' NOT NULL',
            'residence_fees' => Schema::TYPE_FLOAT. ' NOT NULL',
            'meal_plan' => Schema::TYPE_FLOAT. ' NOT NULL'
        ]);
        
        // creates index for column `university_id`
        $this->createIndex(
            'idx-calculation-university',
            'calculatoins',
            'university_id'
        );
        
        // creates index for column `program_id`
        $this->createIndex(
            'idx-calculation-programs',
            'calculatoins',
            'program_id'
        );
        
        // add foreign key for table `calculatoins` and university table
        $this->addForeignKey(
            'fk-calculation-university',
            'calculatoins',
            'university_id',
            'universities',
            'id',
            'RESTRICT'
        );
        
        // add foreign key for table `calculatoins` and programs table
        $this->addForeignKey(
            'fk-calculation-programs',
            'calculatoins',
            'program_id',
            'programs',
            'id',
            'RESTRICT'
        );
        
        
        
        /* INSERT INTIAL DATA  
        include 'DummyData.php';
        // INSERT COUNTRIES 
        $this->execute(DummyData::getCountries());
        // INSERT STATES
        $this->execute(DummyData::getStates());
        // INSERT UNIVERSITIES
        $this->execute(DummyData::getUniversities());
        // INSERT PROGRAMS
        $this->execute(DummyData::getPrograms());
        // INSERT CALCULATION
        $this->execute(DummyData::getCalculations());*/
    }

    public function safeDown()
    {
        echo "m170427_122859_create_country_tbl cannot be reverted.\n";
        
        $this->dropTable('calculatoins');
        $this->dropTable('programs');
        $this->dropTable('universities');
        $this->dropTable('state');
        $this->dropTable('countries');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
