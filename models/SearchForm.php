<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 *  
 *
 * @property integer $university 
 * @property integer $state 
 * @property integer $counry 
 * @property integer $program 
 *
 */
class SearchForm extends Model
{
    public $university;
    public $state;
    public $country=38;
    public $program;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['university', 'program','country'], 'required'],
            ['state','safe']
            // rememberMe must be a boolean value
             
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
         return [
             'university'=>'School',
             'program'=>'Select Program',
             'country'=>'Country',
             'state'=>'Province'
         ];
    }
    

     

     
}
