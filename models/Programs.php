<?php
namespace app\models;

use app\models\Calculatoins;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;



/**
 * This is the model class for table "programs".
 *
 * @property string $id
 * @property string $name
 *
 * @property Calculatoins[] $calculatoins
 */
class Programs extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'programs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCalculatoins()
    {
        return $this->hasMany(Calculatoins::className(), ['program_id' => 'id']);
    }
}
