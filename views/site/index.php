<?php

/* @var $this View */
/* @var $searchform SearchForm */
/* @var $universities Universities */
/* @var $countries Country */
/* @var $price_details Array */

use app\models\Calculatoins;
use app\models\Country;
use app\models\SearchForm;
use app\models\State;
use app\models\Universities;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="site-index">

         <?php $form = ActiveForm::begin(['method'=>'post']); ?>
    
        <?= $form->field($searchform, 'university')->dropDownList(ArrayHelper::map(Universities::find()->orderBy('name')->asArray()->all(),'id', 'name'),['prompt'=>'Select']) ?>
        <?php  
        
            $program =  $form->field($searchform, 'program');
            
            $programs_list = Calculatoins::find()->where(['university_id'=>$searchform->university])->joinWith('program',true)->asArray()->all();
             
            $programs_list =  ArrayHelper::map($programs_list,'id','program');
            $programs_list   =  ArrayHelper::map($programs_list ,'id','name'); 
            
            echo $program->widget(DepDrop::classname(), [
                
                'data' => $programs_list,
                'pluginOptions'=>[
                    'depends'=>['searchform-university'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/site/getprograms'])
                ]
            ]);
        ?>
        <?= $form->field($searchform, 'country')->dropDownList(ArrayHelper::map(Country::find()->orderBy('name')->asArray()->all(),'id', 'name')) ?>
        <?php  
        
            $state =  $form->field($searchform, 'state');
             
            echo $state->widget(DepDrop::classname(), [
                'options'=>['id'=>'state'],
                'data' => ArrayHelper::map(State::find()->where(['country_id'=>$searchform->country])->orderBy('name')->asArray()->all(), 'id', 'name'),
                'pluginOptions'=>[
                    'depends'=>['searchform-country'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/site/getstate'])
                ]
            ]);
        ?>
        
       
         
        <?= Html::submitButton('Get Quote', ['class' => 'btn btn-primary', 'name' => 'submit']) ?>
        <?php ActiveForm::end(); ?>
    
    
    
       
        <?php 
        
        if(Yii::$app->request->post())
        {
         ?>
          <h2>Here You go</h2>
          
          <p><b>Tution Fee</b> - <?=$price_details['tution_fees']?></p>
          <p><b>Tution Fee</b> - <?=$price_details['residence_fees']?></p>
          <p><b>Tution Fee</b> - <?=$price_details['meal_plan']?></p>
          <p><b>Total</b> - <?=array_sum($price_details)?></p>
         <?php 
        }
        ?>
    </div>
</div>
