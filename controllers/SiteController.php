<?php

namespace app\controllers;

use app\models\Calculatoins;
use app\models\ContactForm;
use app\models\Country;
use app\models\LoginForm;
use app\models\SearchForm;
use app\models\State;
use app\models\Universities;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays mani page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $univesities = Universities::find()->All();
        $countries = Country::find()->all();
        $searchform = new SearchForm();
        
        $price_details = [];
        if($searchform->load(Yii::$app->request->post())){
            $tution_fees = 0;
            $calculation = Calculatoins::findOne(['university_id'=>$searchform->university,'program_id'=>$searchform->program]);
            if($calculation)
            {
                $country = Country::findOne($searchform->country);
                $university = Universities::findOne($searchform->university);
                if($country->name == 'Canada')
                { 
                    //var_dump($university->province);
                    //var_dump($searchform->state);
                    if($university->province->id == $searchform->state)
                    {
                         $tution_fees = $calculation->tution_province;
                         
                    }
                    else 
                    {
                        $tution_fees = $calculation->tution_canada;
                    }
                }
                else 
                {
                    $tution_fees = $calculation->tution_international;
                }
                $price_details['tution_fees'] = $tution_fees;
                $price_details['residence_fees']= $calculation->residence_fees;
                $price_details['meal_plan']= $calculation->meal_plan;
            }
            else 
            {
                $searchform->addError('university','Something Went Wrong please try again');
                $searchform->addError('program','Something Went Wrong please try again');
            }
        }
        return $this->render('index',[
            'searchform'=>$searchform,
            'universities'=>$univesities,
            'countries'=>$countries,
            'price_details'=>$price_details
        ]);
    }
    
    /**
     * get state based on country selected
     * @return json 
     */
    public function  actionGetstate()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country_id = $parents[0];
                if($country_id !='')
                {
                    $country = State::findAll(['country_id'=>$country_id]);
                    //var_dump($country);
                      
                    // var_dump($out);
                    // the getSubCatList function will query the database based on the
                    // cat_id and return an array like below:
                    // [
                    //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                    //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                    // ]
                    echo Json::encode(['output'=>$country, 'selected'=>'']);
                    return;
                }
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    
    /**
     * get state based on country selected
     * @return json 
     */
    public function  actionGetprograms()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $university_id = $parents[0];
                if($university_id !='')
                {
                    $programs = Calculatoins::find()->where(['university_id'=>$university_id])->joinWith('program',true)->asArray()->all();
                    $programs =  ArrayHelper::map($programs,'id','program');
                    
                    
                      
                    // var_dump($out);
                    // the getSubCatList function will query the database based on the
                    // cat_id and return an array like below:
                    // [
                    //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                    //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                    // ]
                    echo Json::encode(['output'=>$programs, 'selected'=>'']);
                    return;
                }
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
